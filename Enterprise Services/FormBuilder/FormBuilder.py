from datetime import datetime
import json
from AbstractJMSServices.JMSService import JMSListener
from AbstractJMSServices.AbstractTypes import MessageType, RequestType

first_name = ""
last_name = ""
email_address = ""
converted_results = ""


def start_form_building(msg):
    print("start formbuilding")
    # Beginnen van JSON parsen uit msg, voor de voornaam, achternaam, e-mailadres en vragenlijstresultaten
    if msg:
        print("Request received! - " + str(msg))

    json_msg = json.loads(str(msg))
    global first_name
    first_name = json_msg["voornaam"]
    global last_name
    last_name = json_msg["achternaam"]
    global email_address
    email_address = json_msg["email"]
    raw_survey_results = json_msg["antwoorden_survey"]
    global converted_results
    converted_results = raw_survey_results.split(" | ")

    separator = "|"
    converted_results = separator.join(converted_results)

    jmsListener.send_message(converted_results, RequestType[msg.subject], msg.correlation_id, "ENTERPRISE_SERVICE_2",
                             MessageType.REQUEST, "requestqueue", msg.application_properties)

    # JMS sturen aan Enterprise Service 2 met de vragenlijstresultaten
    # jListener.send_message(msg, RequestType van ontvangen request, corrid uit request, to wordt ENTERPRISE_SERVICE_2,
    # MessageType.REQUEST, senderqueue)


def form_finalisation(msg):
    print("resume formbuilding")
    # Extraheren van aanbevolen AI met beschrijving
    if msg:
        print("Request received! - " + str(msg))

    json_msg = json.loads(str(msg))
    recommended_ai = json_msg["title"]
    recommended_ai_description = json_msg["description"]

    report = {"aanbevelingsrapport": datetime.now().strftime("%d/%m/%y %H:%M:%S"), "voornaam": first_name,
              "achternaam": last_name, "e-mailadres": email_address, "vragenlijstantwoorden": converted_results,
              "aanbevolen AI": recommended_ai, "beschrijving": recommended_ai_description}
    json_report = json.dumps(report)

    jmsListener.send_message(json_report, RequestType[msg.subject], msg.correlation_id, "INTEGRATION_HUB",
                             MessageType.RESPONSE, "requestqueue", msg.application_properties)


jmsListener = JMSListener(start_form_building, form_finalisation)
