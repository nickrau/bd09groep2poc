from azure.servicebus import ServiceBusClient, ServiceBusMessage
from AbstractJMSServices.AbstractTypes import MessageType, RequestType
import threading
import os
from datetime import datetime


class JMSListener(object):
    def __init__(self, startRequestFunc, resumeRequestFunc = None):
        self.CONNECTION_STR = os.environ['CONNSTR']
        self.LISTEN_QUEUE = os.environ['LISTENQUEUE']

        self.startRequestFunc = startRequestFunc
        self.resumeRequestFunc = resumeRequestFunc

        self.servicebus_client = ServiceBusClient.from_connection_string(conn_str=self.CONNECTION_STR,
                                                                         logging_enable=True)
        self.queuelistener = self.servicebus_client.get_queue_receiver(queue_name=self.LISTEN_QUEUE)

        print("JMSListener      : Starting Thread...")
        listenerThread = threading.Thread(target=self.listen, args=(self.queuelistener,))  # Send queue to listener
        listenerThread.start()

    def change_expected_response(self, newResponse):
        self.expected_message = newResponse

    def change_listen_function(self, listenfunction):
        self.listenfunction = listenfunction

    def send_message(self, body: str, subject: RequestType, correlation_id: str, to: str, messagetype: MessageType, sender_queue: str, application_properties: dict = None):
        sender = self.servicebus_client.get_queue_sender(queue_name=sender_queue)
        message = ServiceBusMessage(body)

        if application_properties is not None and "log" in application_properties.keys():
            application_properties['log'] += ";" + message.message_id + ">>" + str(datetime.now())
        else:
            temp_prop = {}
            temp_prop['log'] = message.message_id + ">>" + str(datetime.now())
            application_properties = temp_prop

        message.application_properties = application_properties
        message.to = to
        message.subject = subject.name
        message.reply_to = os.environ['APP_ID']
        message.correlation_id = correlation_id
        message.application_properties['messagetype'] = messagetype.name
        sender.send_messages(message)

        print("JMSListener      : " + to)

    def listen(self, queue):
        with queue:
            print("JMSListener      : Thread Started...")

            for msg in queue:  # Hier komen de messages
                if msg.subject == RequestType.TEST.name:
                    self.send_message("GOT MESSAGE BOSS!!!", RequestType.TEST, msg.correlation_id, msg.reply_to, MessageType.RESPONSE, "responsequeue", msg.application_properties)
                app_props = {key.decode(): val.decode() for key, val in
                             msg.application_properties.items()}  # Decode bytes to string
                if msg.to == os.environ['APP_ID']:
                    if app_props['messagetype'] == MessageType.REQUEST.name:
                        self.startRequestFunc(msg)
                        queue.complete_message(msg)
                    if app_props['messagetype'] == MessageType.RESPONSE.name:
                        if self.resumeRequestFunc is not None:
                            self.resumeRequestFunc(msg)
                        else:
                            print("JMSListener      : RESUME FUNCTION NOT DEFINED. REMOVING MESSAGE FROM QUEUE.")
                        queue.complete_message(msg)
            print("JMSListener      : Stopped Thread...")
