from enum import Enum

class MessageType(Enum):
    RESPONSE = 1
    REQUEST = 2

class RequestType(Enum):
    QA = 1
    TEST = 2