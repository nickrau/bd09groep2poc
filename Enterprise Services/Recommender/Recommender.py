from AbstractJMSServices.JMSService import JMSListener
from AbstractJMSServices.AbstractTypes import MessageType, RequestType
from collections import Counter
import json


def recommender(msg):
    result = most_frequent(str(msg))
    title = 'error'
    description = 'error'
    if result == "A":
        title = "Spaghetti"
        description = "Een lekkere pasta dat ook tot code leidt"
    elif result == "B":
        title = "Macaroni"
        description = "Een lekkere pasta dat ook tot code leidt"
    elif result == "C":
        title = "Lasagne"
        description = "Een lekkere pasta dat ook tot code leidt"
    elif result == "D":
        title = "Penne Pasta"
        description = "Een lekkere pasta dat ook tot code leidt"
    recommendation = {"title": title, "description": description}
    send_this = json.dumps(recommendation)
    jmsListener.send_message(send_this, RequestType[msg.subject], msg.correlation_id, msg.reply_to,
                             MessageType.RESPONSE, "requestqueue", msg.application_properties)


# Functie voor het meest voorkomende onderdeel te vinden.
def most_frequent(survey):
    surveylist = survey.split("|")
    occurrence_count = Counter(surveylist)
    return occurrence_count.most_common(1)[0][0]


# surveymsg = "A|B|C|D|A"
# recommender(surveymsg)

# Gebruik listener queue om informatie op te vangen (in dit geval van de formatter)
jmsListener = JMSListener(recommender)
