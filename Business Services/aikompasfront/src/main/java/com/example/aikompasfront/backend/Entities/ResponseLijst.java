package com.example.aikompasfront.backend.Entities;

public class ResponseLijst {
    public String aanbevelingsrapport;
    public String voornaam;
    public String achternaam;
    public String email;
    public String vragenlijstantwoorden;
    public String aanbevolenAI;
    public String beschrijving;

    public ResponseLijst(String aanbevelingsrapport, String voornaam, String achternaam, String email, String vragenlijstantwoorden, String aanbevolenAI, String beschrijving) {
        this.aanbevelingsrapport = aanbevelingsrapport;
        this.voornaam = voornaam;
        this.achternaam = achternaam;
        this.email = email;
        this.vragenlijstantwoorden = vragenlijstantwoorden;
        this.aanbevolenAI = aanbevolenAI;
        this.beschrijving = beschrijving;
    }
}
