package com.example.aikompasfront.backend.Entities;


public class Vragenlijst {
    public String voornaam;
    public String achternaam;
    public String email;
    public String antwoorden_survey;

    public Vragenlijst(String voornaam, String achternaam, String email, String antwoorden_survey){
        this.voornaam = voornaam;
        this.achternaam = achternaam;
        this.email = email;
        this.antwoorden_survey = antwoorden_survey;
    }

    @Override
    public String toString(){
        return "\"VragenLijst [voornaam = " + voornaam + ",achternaam = " + achternaam + ", email = " + email + ", antwoorden_survey = " + antwoorden_survey + "]";
    }
}
