package com.example.aikompasfront;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AikompasfrontApplication {

    public static void main(String[] args) {
        SpringApplication.run(AikompasfrontApplication.class, args);
    }

}
