package com.example.aikompasfront.ui;

import com.example.aikompasfront.backend.Entities.ResponseLijst;
import com.example.aikompasfront.backend.Entities.Vragenlijst;
import com.example.aikompasfront.backend.BusinessService;
import com.google.gson.Gson;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.radiobutton.RadioGroupVariant;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

@Route("")
@PageTitle("Demo AIKompas")
@CssImport("./styles/mainstyle.css")
public class MainView extends VerticalLayout {

    public List<RadioButtonGroup<String>> vragen = new ArrayList<>();
    public TextField voornaam = new TextField();
    public TextField achternaam = new TextField();
    public EmailField email = new EmailField();
    public VerticalLayout vragenlijstLayout = new VerticalLayout();
    public VerticalLayout waitLayout = new VerticalLayout();
    public VerticalLayout responseLayout = new VerticalLayout();

    public Label voornaamLBL = new Label("");
    public Label achternaamLBL = new Label("");
    public Label emailLBL = new Label("");
    public Label responseLBL = new Label("");
    public Label antwoordenLBL = new Label("");
    public Label beschrijvingLBL = new Label("");

    public BusinessService businessService = new BusinessService();

    public MainView(){
        addClassName("list-view");
        setSizeFull();
        setPadding(true);
        vragenlijstLayout.setSizeFull();
        vragenlijstLayout.setPadding(true);
        waitLayout.setSizeFull();
        waitLayout.setPadding(true);
        waitLayout.add(new Label("WAIT"));
        responseLayout.setSizeFull();
        responseLayout.setPadding(true);
        setResponseLayout();

        Label title = new Label("Demo AIKompas");
        Label smallDesc = new Label("Dit is een demo van het AIKompas. Hieronder vind je een vragenlijst dat na invulling een antwoord gaat geven op de vraag 'Welk AI past bij mijn probleem?'");
        Label disclaimerlabel = new Label("Graag alle vragen en persoonsgegevens invullen om een advies te krijgen.");
        title.setWidthFull();
        title.addClassName("titlelabel");
        smallDesc.addClassName("smalldesclabel");
        disclaimerlabel.addClassName("disclaimerlabel");

        vragenlijstLayout.add(title);
        vragenlijstLayout.add(smallDesc);
        vragenlijstLayout.add(disclaimerlabel);

        setVragenlijst();
        setAlignItems(Alignment.CENTER);
        vragenlijstLayout.setAlignItems(Alignment.CENTER);
        waitLayout.setAlignItems(Alignment.CENTER);
        responseLayout.setAlignItems(Alignment.CENTER);

        waitLayout.setVisible(false);
        responseLayout.setVisible(false);

        add(waitLayout);
        add(vragenlijstLayout);
        add(responseLayout);
    }

    public void setResponseLayout(){
        Label title = new Label("Uw Resultaat!");
        title.addClassName("titlelabel");
        responseLayout.add(title);
        responseLayout.add(voornaamLBL);
        responseLayout.add(achternaamLBL);
        responseLayout.add(emailLBL);
        responseLayout.add(antwoordenLBL);
        responseLayout.add(responseLBL);
        responseLayout.add(beschrijvingLBL);
        Button resetBTN = new Button("Stuur een nieuwe request");
        resetBTN.addClickListener(buttonClickEvent -> resetLayouts());
        responseLayout.add(resetBTN);
    }

    public void fillResponseLayout(String vnaam, String anaam, String email, String antwoorden, String result, String beschrijving){
        voornaamLBL.setText("Voornaam: " + vnaam);
        achternaamLBL.setText("Achternaam: " + anaam);
        emailLBL.setText("Emailadres: " + email);
        antwoordenLBL.setText("Gegeven antwoorden: " + antwoorden);
        responseLBL.setText("Aanbevolen AI: " + result);
        beschrijvingLBL.setText("Beschrijving: " + beschrijving);


    }

    private void setVragenlijst() {
        voornaam.setLabel("Voornaam:");
        achternaam.setLabel("Achternaam:");
        email.setLabel("Email:");

        HorizontalLayout voorENachternaam = new HorizontalLayout();
        voorENachternaam.add(voornaam);
        voorENachternaam.add(achternaam);
        vragenlijstLayout.add(voorENachternaam);
        vragenlijstLayout.add(email);

        setVragen();

        for (RadioButtonGroup<String> vraag : vragen
             ) {
            vragenlijstLayout.add(vraag);
        }

        Button submitBtn = new Button("Geef mij een advies!");
        submitBtn.addClickListener(buttonClickEvent -> {
            Notification.show("Sending request...");
            sendQuestion();
                });
        vragenlijstLayout.add(submitBtn);
    }

    private void setVragen() {
        RadioButtonGroup<String> vraag1 = new RadioButtonGroup<>();
        vraag1.setLabel("Vraag 1");
        vraag1.setItems("A", "B", "C", "D");
        vraag1.addThemeVariants(RadioGroupVariant.LUMO_HELPER_ABOVE_FIELD);


        RadioButtonGroup<String> vraag2 = new RadioButtonGroup<>();
        vraag2.setLabel("Vraag 2");
        vraag2.setItems("A", "B", "C", "D");
        vraag2.addThemeVariants(RadioGroupVariant.LUMO_HELPER_ABOVE_FIELD);


        RadioButtonGroup<String> vraag3 = new RadioButtonGroup<>();
        vraag3.setLabel("Vraag 3");
        vraag3.setItems("A", "B", "C", "D");
        vraag3.addThemeVariants(RadioGroupVariant.LUMO_HELPER_ABOVE_FIELD);


        RadioButtonGroup<String> vraag4 = new RadioButtonGroup<>();
        vraag4.setLabel("Vraag 4");
        vraag4.setItems("A", "B", "C", "D");
        vraag4.addThemeVariants(RadioGroupVariant.LUMO_HELPER_ABOVE_FIELD);


        RadioButtonGroup<String> vraag5 = new RadioButtonGroup<>();
        vraag5.setLabel("Vraag 5");
        vraag5.setItems("A", "B", "C", "D");
        vraag5.addThemeVariants(RadioGroupVariant.LUMO_HELPER_ABOVE_FIELD);


        vragen.add(vraag1);
        vragen.add(vraag2);
        vragen.add(vraag3);
        vragen.add(vraag4);
        vragen.add(vraag5);
    }

    public void resetLayouts(){
        vragenlijstLayout.setVisible(true);
        voornaam.clear();
        achternaam.clear();
        email.clear();
        for (RadioButtonGroup<String> vraag : vragen
             ) {
            vraag.clear();
        }
        responseLayout.setVisible(false);
    }

    public void sendQuestion(){
        String answers = "";
        for (RadioButtonGroup<String> vraag : vragen
             ) {
            answers += vraag.getValue() + " | ";
        }

        answers = answers.substring(0, answers.length() - 2);

        Vragenlijst vlijst = new Vragenlijst(voornaam.getValue(), achternaam.getValue(), email.getValue(), answers);

        HttpResponse response = businessService.GetQA(vlijst);
        String res = response.body().toString();
        res = res.replace("e-mailadres", "email");
        res = res.replace("aanbevolen AI", "aanbevolenAI");

        ResponseLijst responseLijst = new Gson().fromJson(res, ResponseLijst.class); // TODO Fixen. Moet naar Responselijst class
        fillResponseLayout(responseLijst.voornaam, responseLijst.achternaam, responseLijst.email, responseLijst.vragenlijstantwoorden, responseLijst.aanbevolenAI, responseLijst.beschrijving);
        vragenlijstLayout.setVisible(false);
        responseLayout.setVisible(true);
    }


}
