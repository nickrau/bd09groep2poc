package com.example.aikompasfront.backend;

import com.example.aikompasfront.backend.Entities.Vragenlijst;
import com.google.gson.Gson;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Service
public class BusinessService {

    public HttpResponse GetQA(Vragenlijst vragenlijst){
        try {
            var client = HttpClient.newHttpClient();

            String json = new Gson().toJson(vragenlijst);
            var request = HttpRequest.newBuilder(
                    URI.create("http://aiorchestrator.azurewebsites.net/api/aikompas?requesttype=QA&code=5uLEMTh/ijZg9SQP8O2yQRH1s2VKFW9BwSpxbavQBTbeiVZcHulVWQ=="))
                    .header("accept", "*/*")
                    .header("Content-Type", "application/json")
                    .POST(HttpRequest.BodyPublishers.ofString(json))
                    .build();


            var response = client.send(request, HttpResponse.BodyHandlers.ofString());
            System.out.println(response.body());
            return response;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
