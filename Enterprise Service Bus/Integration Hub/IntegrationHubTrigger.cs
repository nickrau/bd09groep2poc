using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Azure.Messaging.ServiceBus;

namespace Company.Function
{
    public static class IntegrationHubTrigger
    {
        public enum MessageTypes{
            REQUEST,
            RESPONSE
        }

        public static string enterprisequeue; // TODO: Has to be get dynamically through database
        public static string reciever; // TODO: Has to be get dynamically through database

        [FunctionName("IntegrationHubTrigger")]
        public static async Task Run([ServiceBusTrigger("requestqueue", Connection = "aikomp_SERVICEBUS")] //TODO: Invullen connectionstring door environment_variable
        string msg,
        Int32 deliveryCount,
        DateTime enqueuedTimeUtc,
        string messageId,
        string to,
        string replyto,
        IDictionary<string, object> userProperties,
        string label,
        string correlationid,
        ILogger logger)
        {
            logger.LogInformation("New Request Found");
            logger.LogInformation($"Label: {label}");
            logger.LogInformation($"CorrelationID: {correlationid}");
            logger.LogInformation($"To: {to}");
            logger.LogInformation($"Reply To: {replyto}");
            logger.LogInformation($"Log: {userProperties["log"].ToString()}");
            logger.LogInformation($"MessageType: {userProperties["messagetype"].ToString()}");
            logger.LogInformation($"Message: {msg}\n----------");

            // LOGIC
            switch(label.ToUpper()){
                case "QA":
                    QAChecker(replyto, to, userProperties);
                    break;

                case "TEST":
                    if (to.Contains("ORCHESTRATION_HUB")){
                        reciever = to;
                        enterprisequeue = "responsequeue";
                    }
                    else if(to.Contains("ENTERPRISE_SERVICE_1")){
                        reciever = to;
                        enterprisequeue = "enterprise_service_1_queue";
                    }
                    else if(to.Contains("ENTERPRISE_SERVICE_2")){
                        reciever = to;
                        enterprisequeue = "enterprise_service_2_queue";
                    }
                    break;
                default:
                    reciever = "";
                    enterprisequeue = "";
                    break;
            }

            // END OF LOGIC

            // Setup requestmessage for enterprise service if possible
            try{
                logger.LogInformation("Creating message");

                if(enterprisequeue != string.Empty && reciever != string.Empty){
                    // Set connectionstring
                    string connstr = "CONNECTIONSTRING";//TODO: Invullen connectionstring door environment_variable

                    // Set client and sender
                    ServiceBusClient client = new ServiceBusClient(connstr);
                    ServiceBusSender sender = client.CreateSender(enterprisequeue);

                    // Create new message
                    ServiceBusMessage newMsg = new ServiceBusMessage(msg);
                    // Generate new UUID
                    newMsg.MessageId = Guid.NewGuid().ToString();
                    
                    // Add other variables
                    newMsg.To = reciever;
                    newMsg.ReplyTo = replyto;
                    newMsg.Subject = label;
                    newMsg.CorrelationId = correlationid;
                    
                    // Set UserProperties
                    foreach(var property in userProperties){
                        if(property.Key == "log"){
                            newMsg.ApplicationProperties.Add(property.Key.ToString(), property.Value.ToString() + ";" + newMsg.MessageId + ">>" + DateTime.Now.ToString("yyyy-mm-dd hh:mm:ss"));
                        }
                        else{
                            newMsg.ApplicationProperties.Add(property.Key.ToString(), property.Value.ToString());
                        }
                    }

                    await sender.SendMessageAsync(newMsg);
                    logger.LogInformation("Message sent to: " + newMsg.To + " on " + enterprisequeue + " with body " + newMsg.ToString()+"\n\n\n\n\n");
                    
                }
            } catch (Exception e){
                logger.LogInformation("MESSAGE NOT SEND DUE TO ERROR: "+e.StackTrace);
            }

        }

        static void QAChecker(string replyto, string to, IDictionary<string, object> userProperties){
            MessageTypes messageType = (MessageTypes)Enum.Parse(typeof(MessageTypes),userProperties["messagetype"].ToString());

            switch(messageType){
                case MessageTypes.REQUEST:
                    if(replyto.Contains("ORCHESTRATION_HUB")){
                        SetEnterpriseService("ENTERPRISE_SERVICE_1", MessageTypes.REQUEST);
                    } else{
                        SetEnterpriseService(to, MessageTypes.REQUEST);
                    }
                    break;

                case MessageTypes.RESPONSE:
                    if(to.Contains("ENTERPRISE_SERVICE")){
                        SetEnterpriseService(to, MessageTypes.RESPONSE);
                    } else {
                        reciever = "ORCHESTRATION_HUB";
                        enterprisequeue = "responsequeue";
                    }
                    break;
            }
        }

        static void SetEnterpriseService(string checkVar, MessageTypes messageType){
            switch(checkVar){
                case string a when a.Contains("ENTERPRISE_SERVICE_1"):
                    if(messageType == MessageTypes.REQUEST){
                        reciever = "ENTERPRISE_SERVICE_1-1"; // Vind next available TODO: Maak functie voor vinden next available met bijbehorende queue
                    } else {
                        reciever = checkVar;
                    }
                    enterprisequeue = "enterprise_service_1_queue"; // Vind queue van ENTERPRISE SERVICE
                    break;

                case string a when a.Contains("ENTERPRISE_SERVICE_2"):
                    if(messageType == MessageTypes.REQUEST){
                        reciever = "ENTERPRISE_SERVICE_2-1"; // Vind next available TODO: Maak functie voor vinden next available met bijbehorende queue
                    } else {
                        reciever = checkVar;
                    }
                    enterprisequeue = "enterprise_service_2_queue"; // Vind queue van ENTERPRISE SERVICE
                    break;
            }
            
        }

    }
}
