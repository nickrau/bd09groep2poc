using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Extensions.OpenApi.Core.Attributes;
using Microsoft.Azure.WebJobs.Extensions.OpenApi.Core.Enums;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Azure.Messaging.ServiceBus;

namespace Company.Function
{
    public static class OrchestrationEngineAPITrigger
    {
        public static string connstr = "CONNECTIONSTRING"; //TODO: Invullen connectionstring door environment_variable
        public static Boolean recievedResponse = false;
        public static ServiceBusMessage response;
        public static ServiceBusMessage request;
        public static DateTime start;
        public static ILogger logger;
        
        enum MessageTypes{
            REQUEST,
            RESPONSE
        }

        [FunctionName("OrchestrationEngineAPITrigger")]
        [OpenApiOperation(operationId: "Run", tags: new[] { "Request AI Kompas" }, Description = "Voer request uit naar AI Kompas.")]
        [OpenApiSecurity("function_key", SecuritySchemeType.ApiKey, Name = "code", In = OpenApiSecurityLocationType.Query)]
        [OpenApiParameter(name: "requesttype", In = ParameterLocation.Query, Required = true, Description = "Type Request. Keuze uit QA & TEST")]
        [OpenApiParameter(name: "to", In = ParameterLocation.Query, Required = false, Description = "Alleen voor RequestType TEST. APP_ID waar de test naartoe gaat.")]
        [OpenApiResponseWithBody(statusCode: HttpStatusCode.OK, contentType: "text/plain", bodyType: typeof(string), Description = "The OK response")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = "aikompas")]
            HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");
            logger = log;
            
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            string requesttype = req.Query["requesttype"];
            string receiver = req.Query["to"];

            // Setup JMSMessage en sturen naar queue
            try
            {
                string connectionstring = connstr;

                // Set client and sender
                ServiceBusClient client = new ServiceBusClient(connectionstring);
                ServiceBusSender sender = client.CreateSender("requestqueue");

                // Create new message
                request = new ServiceBusMessage(requestBody);
                logger.LogInformation("Created new message... filling with data");
                // Generate new UUID
                request.MessageId = Guid.NewGuid().ToString();
                // Generate Correlation ID
                request.CorrelationId = Guid.NewGuid().ToString();
                // Set Subject
                request.Subject = requesttype;
                // Set Reply To
                request.ReplyTo = "ORCHESTRATION_HUB";
                // Set Application Properties
                request.ApplicationProperties.Add("log", request.MessageId + ">>" + DateTime.Now.ToString("yyyy-mm-dd hh:mm:ss"));
                request.ApplicationProperties.Add("messagetype", MessageTypes.REQUEST.ToString());
                
                if(requesttype == "TEST"){
                    request.To = "ORCHESTRATOR";
                }

                log.LogInformation("Sending message...");

                // JMSMessage versturen
                await sender.SendMessageAsync(request);

            } catch (Exception e){
                log.LogInformation($"ERROR: {e.StackTrace}");
            }
                start = DateTime.Now;
                recievedResponse = false;
                // Wachten op response
                await WaitForResponse();
                

            // Response terugsturen
            string responseString = response.Body.ToString();
            Console.WriteLine($"Recieved Message Back with body {response.Body.ToString()}");
            Console.WriteLine($"CorrelationID: {response.CorrelationId}");
            // TODO: Send CorrelationID and Log back in HEADERS
            return new OkObjectResult(responseString);
        }

        // handle received messages
        static async Task MessageHandler(ProcessMessageEventArgs args)
        {
            if(args.Message.CorrelationId == request.CorrelationId){

                string bodystring = args.Message.Body.ToString();
                logger.LogInformation($"Recieved: {bodystring}");

                // complete the message. messages is deleted from the queue.
                response = new ServiceBusMessage(bodystring);
                foreach(var property in args.Message.ApplicationProperties){
                    response.ApplicationProperties.Add(property.Key.ToString(), property.Value.ToString());
                }
                response.ContentType = args.Message.ContentType;
                response.CorrelationId = args.Message.CorrelationId;
                response.MessageId = args.Message.MessageId;
                response.PartitionKey = args.Message.PartitionKey;
                response.ReplyTo = args.Message.ReplyTo;
                response.ReplyToSessionId = args.Message.ReplyToSessionId;
                response.ScheduledEnqueueTime = args.Message.ScheduledEnqueueTime;
                response.SessionId = args.Message.SessionId;
                response.Subject = args.Message.Subject;
                response.TimeToLive = args.Message.TimeToLive;
                response.To = response.To;
                
                
                
                await args.CompleteMessageAsync(args.Message);
                recievedResponse = true;
            }
        }

        // handle any errors when receiving messages
        static Task ErrorHandler(ProcessErrorEventArgs args)
        {
            logger.LogInformation(args.Exception.ToString());
            return Task.CompletedTask;
        }

        static async Task WaitForResponse(){
            // Create Client
            ServiceBusClient client = new ServiceBusClient(connstr);
            // Create processor to handle messages
            ServiceBusProcessor processor = client.CreateProcessor("responsequeue", new ServiceBusProcessorOptions());

            try{
                // Handler to proces messages
                processor.ProcessMessageAsync += MessageHandler;

                // Handler to process errors
                processor.ProcessErrorAsync += ErrorHandler;

                await processor.StartProcessingAsync();

                logger.LogInformation("Waiting for response");
                
                while(recievedResponse == false){
                    
                    Console.WriteLine(start - DateTime.Now);
                }

                logger.LogInformation("Stopping reciever");
                await processor.StopProcessingAsync();
                logger.LogInformation("Reciever stopped");

            } finally {
                // Calling DisposeAsync on client types to clean up mess
                await processor.DisposeAsync();
                await client.DisposeAsync();
            }
        }
    }
}

