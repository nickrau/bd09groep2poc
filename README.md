# BD09 Groep 2 Proof of Concept

## Inhoudsopgave
- [BD09 Groep 2 Proof of Concept](#bd09-groep-2-proof-of-concept)
  * [Installatie](#installatie)
  * [Gebruik](#gebruik)
  * [Voor toevoegen van nieuwe onderdelen/commits](#voor-toevoegen-van-nieuwe-onderdelen-commits)
  * [Nieuwe Enterprise Service toevoegen](#nieuwe-enterprise-service-toevoegen)
  * [TO-DO](#to-do)
  * [Licentie](#licentie)

  
## Beschrijving
Een Proof of Concept voor het *AI Compass* project. Dit betreft een project voor de minor *BD09 Software Architecture* 
op *Zuyd Hogeschool*. 

Het projectgroep (groep 2) gedurende de schoolopdracht bestaat uit de volgende studenten:

- Luc Quaedvlieg
- Nick Rau
- Kevin Wijnen

## Installatie

- Maak Docker Container voor Enterprise Service aan
- Orchestration Engine en Integration Hub pushen naar Microsoft Azure
- Frontend/Business Service initialiseren met API-verbinding naar de Orchestration Engine
- Geef de Azure Functions authenticatie code bij HTTP-triggers
- Voor de Business Service, gebruik de volgende endpoint: `<webadres>/api/aikompas?requesttype=QA

## Gebruik

- Initialiseer het Azure architectuur
- Ga naar de gehoste website/Business Service en vul de vragenlijst in
- Een response wordt teruggegeven in de vorm van een aanbevelingsrapport

## Voor toevoegen van nieuwe onderdelen/commits

- Maak een fork van het project
- Maak nieuwe branche voor de functionaliteit die je wilt toevoegen
- Maak merge request aan voor de gesuggereerde commits, zodat het beoordeeld kan worden


## Nieuwe Enterprise Service toevoegen

Voor het toevoegen van een nieuwe Enterprise Service, is besloten om volgens de volgende stappen het te implementeren
in je eigen *fork*:

- Maak nieuwe map in de *root*-map met de naam van de Enterprise Service
- Plaats je `.py` bestanden in de nieuwe map.
- Importeer de JMSListener door middel van ` from AbstractJMSServices.JMSService import JMSListener`(waar nodig) in de
  code
- Importeer de `from AbstractJMSServices.AbstractTypes import MessageType, RequestType` (waar nodig) in de code

## TO-DO

- [X] Maken van **StartRequest** functie voor
	- berekening(en)
	- in de helft van de berekening, wordt een request gestuurd naar de main queue voor informatie vragen
	- Sla de request tijdelijk op (als bestand, database bijvoorbeeld)
	- Wacht op de request (doorgeven dat request opgehaald kunnen worden)
- [X] Maken van **ResumeRequest** functie voor
	+ Opzoeken naar antwoord van de StartRequest functie
	+ Maak berekening af
	+ Stuur de *response* terug via de *REPLY_TO*-bestemming

## Licentie

*BD09 Groep 2 Proof of Concept* is released under the [MIT License](http://www.opensource.org/licenses/MIT).
